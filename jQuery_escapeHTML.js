// 参考 http://stackoverflow.com/questions/6020714/escape-html-using-jquery

escapeHTML = function(text) {
  return $('<div/>').text(text).html();
}

escapeHTML('<div id="main">');
// "&lt;div id="main"&gt;"